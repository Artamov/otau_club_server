<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 019 19.09.18
 * Time: 14:02
 */

namespace App\Library;


class RESTAnswer
{
    public static function addFailed($mess)
    {
        $data = [
            'status' => 'failed',
            'message' => $mess
        ];
        return response(
            $data,
            400
        )
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Content-Type', 'application/json');

    }
    public static function addSuccess($data)
    {
        return response(
            $data,
            200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->header('Content-Type', 'application/json');

    }
}