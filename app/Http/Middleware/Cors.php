<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 025 25.10.18
 * Time: 17:30
 */

namespace App\Http\Middleware;
use Closure;



class Cors
{
    public function handle($request, Closure $next)
    {
        return $next($request)->header('Access-Control-Allow-Origin' , '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, PATCH, HEAD')
            ->header('Access-Control-Allow-Headers', 'Authorization');
    }
}