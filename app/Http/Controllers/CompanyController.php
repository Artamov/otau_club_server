<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use Illuminate\Http\Request;


class CompanyController extends Controller
{
    public function categories()
    {
        $categories = Category::whereNull('category_id')->get();

        return view('categories', compact('categories'));
    }

    public function getAll(Request $request)
    {
        return Company::getAll();
    }
    public function get(Request $request)
    {
        $id = $request->input('id');
        return Company::getId($id);
    }
    public function add(Request $request)
    {

    }
    public function category(Request $request)
    {
        $category = $request->input('category_id');
        return Company::getByCategory($category);
    }
    public static function main()
    {
//        $main = [];
//        $cat = Category::test();
//        for($i = 0; $i <= count($cat)-1; $i++ ){
//            if($cat[$i]->category_id == null) {
//                $main[] = $cat[$i];
//            }
//        }
//        return $main;
    }
    public function test()
    {
        $rr = Category::test()->toArray();
        
        $sad = [];

        for ($i = 0; $i < count($rr); $i++) {
            $rrI = (array)$rr[$i];
            //var_dump($rrI);
            if ($rrI['category_id'] !== null) {
                $sad[$rrI['category_id']]['childes'][] = $rrI;
            } else {
                $sad[$rrI['id']] = $rrI;
            }

        }
        return array_values($sad);

    }
    public static function search(Request $request)
    {
        $word = $request->get('word');
        var_export(Company::search($word));
    }

    public static function getCompanySlider()
    {

    }
}
