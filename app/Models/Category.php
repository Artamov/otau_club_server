<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Category extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'title', 'category_id'];

    public function subCategory()
    {
        return $this->hasMany('App\Category');
    }

    public static function getAll()
    {
        return DB::table('categories')->get();
    }
    public static function test()
    {
        $cat = DB::table('categories')->get();
        return $cat;
    }

}
