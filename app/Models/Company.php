<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    private $title;
    private $category_id;
    private $logo;
    private $breaf_description;
    private $phone;
    private $address;
    private $city_id;

    public function __construct($title, $category_id, $logo,
                                $breaf_description, $phone, $address, $city_id)
    {
        $this->title = $title;
        $this->category_id = $category_id;
        $this->logo = $logo;
        $this->breaf_description = $breaf_description;
        $this->phone = $phone;
        $this->address = $address;
        $this->city_id = $city_id;
    }

    public function add()
    {
        return DB::table('companies')->insert([
            'title' => $this->title,
            'category_id' => $this->category_id,
            'logo' => $this->logo,
            'breaf_description' => $this->breaf_description,
            'phone' => $this->phone,
            'address' => $this->address,
            'city_id' => $this->city_id,
        ]);
    }

    public static function checkCompany(Company $company, $count)
    {
        $i = 0;
        foreach($company as $field => $val){
            $i++;
            if(empty($val)){
                return false;
            }
            if($i == $count){
                return true;
            }
        }
        return true;
    }

    public static function getAll()
    {
        return DB::table('companies')->get();
    }
    public static function getId($id)
    {
        return DB::table('companies')->where('id', '=', $id)->get();
    }
    public function isUniqueTitle()
    {
        if((DB::table('companies')->where('title', '=', $this->title)->get()->count()) == 0){
            return true;
        }
        return false;
    }

    public function isUniquePhone()
    {
        if((DB::table('companies')->where('phone',  '=', $this->phone)->get()->count()) == 0){
            return true;
        }
        return false;
    }

    public static function isExist($id)
    {
        if((DB::table('categories')->where('id', $id)->get()->count()) != 0){
            return true;
        }
        return false;
    }

    public static function del($id)
    {
        return DB::table('categories')->where('id', '=', $id)->delete();
    }

    public static function getByCategory($category)
    {
        return DB::table('companies')->where('category_id', '=', $category)->get();
    }

    public static function test()
    {

    }
    public static function search($word)
    {
        return DB::table('companies')->where('title', 'LIKE', "%$word%")->get();
    }


}
