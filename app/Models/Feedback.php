<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 026 26.09.18
 * Time: 10:49
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Feedback extends Model
{
    public static function add($name, $email, $phone, $subject, $message)
    {
        return DB::table('feedback')->insert([
            'user_name' => $name,
            'user_email' => $email,
            'user_phone' => $phone,
            'subject' => $subject,
            'message' => $message
        ]);
    }
    public static function getAll()
    {
        return DB::table('feedback')->get();
    }
}