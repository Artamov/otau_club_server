<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    echo "index page";
});

use App\Library\RESTAnswer;

Route::get('/category', 'CompanyController@categories');
//Route::post('/company/add', 'CompanyController@add');


Route::get('/categories/get', function (Request $request) {
    //$token = $request->bearerToken();
    return RESTAnswer::addSuccess(\App\Category::getAll());
});

Route::get('/company/get', 'CompanyController@get');
Route::get('qr', 'PaymentController@qr');
Route::get('pay', 'PaymentController@pay');
Route::get('/test', 'ContactsController@test');
Route::get('/company/get_all', 'CompanyController@getAll');
Route::get('/company/test', 'CompanyController@test');
Route::post('/company/category', 'CompanyController@category');
Route::get('/company/search', 'CompanyController@search');
Route::get('/contacts', 'ContactsController@feedback');