<?php

use Illuminate\Http\Request;
use \App\Library\RESTAnswer;
use App\library\JWT;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/****************** CATEGORIES ****************/




/*************** COMPANIES **********************/

Route::get('/companies/get', function(Request $request){
    $token = $request->bearerToken();
    return RESTAnswer::addSuccess(\App\Company::getAll());
});

Route::post('/company/add', function(Request $request){
    $token = $request->bearerToken();

    $company = new \App\Company(
        $request->json('title'),
        $request->json('category_id'),
        $request->json('logo'),
        $request->json('breaf_description'),
        $request->json('phone'),
        $request->json('address'),
        $request->json('city_id')
    );
    if(!\App\Company::checkCompany($company, 7)){
        return RESTAnswer::addFailed('Empty field');
    }else {

        if(!$company->isUniqueTitle()){
            return RESTAnswer::addFailed( 'Titile not unique');

        }else if(!$company->isUniquePhone()){
            return RESTAnswer::addFailed('Phone not unique');
        }
        if($company->add()) {
            return RESTAnswer::addSuccess(['success', 'Company added']);
        }
    }

});

Route::post('/company/del', function(Request $request){
    $id = $request->id;
    var_export($id);
    if(empty($id)){
        return RESTAnswer::addFailed('Empty id field!');
    }else {
        if(\App\Company::isExist($id)){
            if(true){
                var_export(\App\Company::del($id));
                return RESTAnswer::addSuccess(['message' => 'company deleted']);
            }
        }else {
            return RESTAnswer::addFailed('company not found');
        }
    }
});


/******************* TEST *********************/

Route::post('/db', function(Request $request){
    $id = $request->id;

});

Route::get('/test', function(Request $request){
    echo "fdfd";
    $token = $request->bearerToken();

    var_export(JWT::JWTDecode($token));

});

Route::post('/token', function(Request $request){
    $id = 42;
    echo JWT::JWTEncode(['id' => $id]);
});
