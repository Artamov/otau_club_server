<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Categories</title>
   <h1>Test</h1>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>

<body>
<script>
    function tree(menu){
        var main_cat = [];

        for(var i = 0; i <= menu.length-1; i++){
            if(menu[i].category_id == null){
                main_cat.push(menu[i]);
            }
        }
        for(var j = 0; j <= main_cat.length-1; j++) {
            var el = '<div class="cat"><h3>' + main_cat[j].title + '</h3></div>';
            $('body').append(el);
            for(var b = 0; b <= menu.length-1; b++){
                if(main_cat[j].id == menu[b].category_id){
                    var el = '<p>--------' + menu[b].title +'</p>'
                    $('body').append(el);
                }

            }
        }
        console.log(main_cat);
    }

    $(function(){
        $.ajax({
            url: 'http://otau/categories/get',
            type: 'GET',
            dataType: 'html',
            data: {},
        })
            .done(function(data) {
                console.log("success");
                var menu = JSON.parse(data);
                tree(menu);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    });



</script>
</body>
</html>
