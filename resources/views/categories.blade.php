<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Categories</title>
    @foreach ($categories as $category)
        <h3>{{ $category->title }}</h3>
        @foreach ($category->subCategory as $sub)
            ---- <em>{{$sub->title}}</em><br>
        @endforeach
        <br>
    @endforeach

</head>
<body>

</body>
</html>
