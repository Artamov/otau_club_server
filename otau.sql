-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 09 2018 г., 08:41
-- Версия сервера: 5.7.19
-- Версия PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `otau`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `icon`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Еда', '/img/1.png', NULL, NULL, NULL),
(2, 'Магазины', '/img/2.png', NULL, NULL, NULL),
(3, 'Красота и здоровье', '/img/3.png', NULL, NULL, NULL),
(4, 'Досуг и развлечение', '/img/4.png', NULL, NULL, NULL),
(5, 'Сфера услуг', '/img/5.png', NULL, NULL, NULL),
(6, 'Авто', '/img/6.png', NULL, NULL, NULL),
(7, 'Спорт и туризм', '/img/7.png', NULL, NULL, NULL),
(8, 'Образование', '/img/8.png', NULL, NULL, NULL),
(9, 'Строительство и ремонт', '/img/9.png', NULL, NULL, NULL),
(10, 'Финансы и юрисприденция', '/img/10.png', NULL, NULL, NULL),
(11, 'B2B', '/img/11.png', NULL, NULL, NULL),
(17, 'Доставка еды', '', 1, NULL, NULL),
(18, 'Кафе', '', 1, NULL, NULL),
(19, 'Кейтеринг', '', 1, NULL, NULL),
(20, 'Кофейни', '', 1, NULL, NULL),
(21, 'Кулинарии', '', 1, NULL, NULL),
(22, 'Пекарни/кондитерские', '', 1, NULL, NULL),
(23, 'Пицерии', '', 1, NULL, NULL),
(24, 'Продуктовые магазины', '', 1, NULL, NULL),
(25, 'Рестораны', '', 1, NULL, NULL),
(26, 'Столовые', '', 1, NULL, NULL),
(27, 'Суши-бары\r\n', '', 1, NULL, NULL),
(28, 'Фастфуд\r\n', '', 1, NULL, NULL),
(29, 'Аксессуары\r\n', '', 2, NULL, NULL),
(30, 'Бытовая техника', '', 2, NULL, NULL),
(31, 'Все для детей', '', 2, NULL, NULL),
(32, 'Все для дома', '', 2, NULL, NULL),
(33, 'Зоомагазины', '', 2, NULL, NULL),
(34, 'Инструменты', '', 2, NULL, NULL),
(35, 'Интернет-магазин', '', 2, NULL, NULL),
(36, 'Канцелярские товары', '', 2, NULL, NULL),
(37, 'Книги', '', 2, NULL, NULL),
(38, 'Косметика и парфюмерия\r\n', '', 2, NULL, NULL),
(39, 'Мебель\r\n', '', 2, NULL, NULL),
(40, 'Одежда и обувь\r\n', '', 2, NULL, NULL),
(41, 'Окна и двери\r\n', '', 2, NULL, NULL),
(42, 'Оптика\r\n', '', 2, NULL, NULL),
(43, 'Ортопедические товары\r\n', '', 2, NULL, NULL),
(44, 'Охота и рыбалка\r\n', '', 2, NULL, NULL),
(45, 'Пиротехника\r\n', '', 2, NULL, NULL),
(46, 'Подарки\r\n', '', 2, NULL, NULL),
(47, 'Посуда\r\n', '', 2, NULL, NULL),
(48, 'Продуктовые магазины\r\n', '', 2, NULL, NULL),
(49, 'Салоны связи\r\n', '', 2, NULL, NULL),
(50, 'Свадебные салоны\r\n', '', 2, NULL, NULL),
(51, 'Спецодежда\r\n', '', 2, NULL, NULL),
(52, 'Спортивный инвентарь\r\n', '', 2, NULL, NULL),
(53, 'Текстиль\r\n', '', 2, NULL, NULL),
(54, 'Флористические салоны\r\n', '', 2, NULL, NULL),
(55, 'Фотоцентры\r\n', '', 2, NULL, NULL),
(56, 'Электроника\r\n', '', 2, NULL, NULL),
(57, 'Электротовары\r\n', '', 2, NULL, NULL),
(58, 'Ювелирные изделия\r\n', '', 2, NULL, NULL),
(59, 'Аптеки\r\n', '', 3, NULL, NULL),
(60, 'Гомеопатия\r\n', '', 3, NULL, NULL),
(61, 'Косметика и парфюмерия\r\n', '', 3, NULL, NULL),
(62, 'Косметология, эстетическая медицина\r\n', '', 3, NULL, NULL),
(63, 'Массажные кабинеты\r\n', '', 3, NULL, NULL),
(64, 'Медицинские центры\r\n', '', 3, NULL, NULL),
(65, 'Медицинское оборудование\r\n', '', 3, NULL, NULL),
(66, 'Ортопедические товары\r\n', '', 3, NULL, NULL),
(67, 'Парикмахерские\r\n', '', 3, NULL, NULL),
(68, 'Салоны красоты\r\n', '', 3, NULL, NULL),
(69, 'Соляные комнаты\r\n', '', 3, NULL, NULL),
(70, 'Стоматологии\r\n', '', 3, NULL, NULL),
(71, 'Студии загара\r\n', '', 3, NULL, NULL),
(72, 'Студии маникюра\r\n', '', 3, NULL, NULL),
(73, 'Тату-салоны\r\n', '', 3, NULL, NULL),
(74, 'Уход за телом\r\n', '', 3, NULL, NULL),
(75, 'Центры оптики\r\n', '', 3, NULL, NULL),
(76, 'Аквапарки\r\n\r\n', '', 4, NULL, NULL),
(77, 'Антикафе\r\n', '', 4, NULL, NULL),
(78, 'Банкетные залы\r\n', '', 4, NULL, NULL),
(79, 'Бильярдные залы\r\n', '', 4, NULL, NULL),
(80, 'Боулинг\r\n', '', 4, NULL, NULL),
(81, 'Детские клубы\r\n', '', 4, NULL, NULL),
(82, 'Звук и свет\r\n', '', 4, NULL, NULL),
(83, 'Игровые залы\r\n', '', 4, NULL, NULL),
(84, 'Караоке \r\n', '', 4, NULL, NULL),
(85, 'Квесты\r\n', '', 4, NULL, NULL),
(86, 'Кейтеринг\r\n', '', 4, NULL, NULL),
(87, 'Кинотеатры\r\n', '', 4, NULL, NULL),
(88, 'Книги\r\n', '', 4, NULL, NULL),
(89, 'Культура,искусство\r\n', '', 4, NULL, NULL),
(90, 'Ночные клубы\r\n', '', 4, NULL, NULL),
(91, 'Охота и рыбалка\r\n', '', 4, NULL, NULL),
(92, 'Парки аттракционов\r\n', '', 4, NULL, NULL),
(93, 'Праздники\r\n', '', 4, NULL, NULL),
(94, 'Продажа билетов\r\n', '', 4, NULL, NULL),
(95, 'Развлекательные центры\r\n', '', 4, NULL, NULL),
(96, 'Сауны\r\n', '', 4, NULL, NULL),
(97, 'Спорт\r\n', '', 4, NULL, NULL),
(98, 'Спортивно-тактические клубы\r\n', '', 4, NULL, NULL),
(99, 'Творческие мастерские\r\n', '', 4, NULL, NULL),
(100, 'Туризм\r\n', '', 4, NULL, NULL),
(101, 'Туроператоры\r\n', '', 4, NULL, NULL),
(102, 'Фото-видео съемка\r\n', '', 4, NULL, NULL),
(103, 'Хобби\r\n', '', 4, NULL, NULL),
(104, 'Ателье\r\n', '', 5, NULL, NULL),
(105, 'Ветеринарсике клиники\r\n', '', 5, NULL, NULL),
(106, 'Доставка воды\r\n', '', 5, NULL, NULL),
(107, 'Заправка картриджей\r\n', '', 5, NULL, NULL),
(108, 'Зоогостиница\r\n', '', 5, NULL, NULL),
(109, 'Кейтеринг\r\n', '', 5, NULL, NULL),
(110, 'Клининг\r\n', '', 5, NULL, NULL),
(111, 'Логистика\r\n', '', 5, NULL, NULL),
(112, 'Парикмахерские\r\n', '', 5, NULL, NULL),
(113, 'Прачечные\r\n', '', 5, NULL, NULL),
(114, 'Прокат авто\r\n', '', 5, NULL, NULL),
(115, 'Прочие услуги\r\n', '', 5, NULL, NULL),
(116, 'Ремонт бытовой техники\r\n', '', 5, NULL, NULL),
(117, 'Ремонт компьютеров\r\n', '', 5, NULL, NULL),
(118, 'Ремонт оргтехники\r\n', '', 5, NULL, NULL),
(119, 'Ремонт часов\r\n', '', 5, NULL, NULL),
(120, 'Салоны красоты\r\n', '', 5, NULL, NULL),
(121, 'Салоны связи\r\n', '', 5, NULL, NULL),
(122, 'Свадебные салоны\r\n', '', 5, NULL, NULL),
(123, 'Телекоммуникация\r\n', '', 5, NULL, NULL),
(124, 'Химчистки\r\n', '', 5, NULL, NULL),
(125, 'Ремонт спортивного оборудования\r\n', '', 5, NULL, NULL),
(126, 'Автоаксессуары\r\n', '', 6, NULL, NULL),
(127, 'Автодилеры\r\n', '', 6, NULL, NULL),
(128, 'Автозаправки\r\n', '', 6, NULL, NULL),
(129, 'Автозвук\r\n', '', 6, NULL, NULL),
(130, 'Автомагазины\r\n', '', 6, NULL, NULL),
(131, 'Автомойки\r\n', '', 6, NULL, NULL),
(132, 'Автосервисы\r\n', '', 6, NULL, NULL),
(133, 'Автошколы\r\n', '', 6, NULL, NULL),
(134, 'Автоюристы\r\n', '', 6, NULL, NULL),
(135, 'Грузоперевозки\r\n', '', 6, NULL, NULL),
(136, 'Парковки\r\n', '', 6, NULL, NULL),
(137, 'Прокат авто\r\n', '', 6, NULL, NULL),
(138, 'Такси\r\n', '', 6, NULL, NULL),
(139, 'Тюнинг\r\n', '', 6, NULL, NULL),
(140, 'Шиномонтаж\r\n', '', 6, NULL, NULL),
(141, 'Активный отдых\r\n', '', 7, NULL, NULL),
(142, 'Гостиницы\r\n', '', 7, NULL, NULL),
(143, 'Картинг/Мотоцентры\r\n', '', 7, NULL, NULL),
(144, 'Ремонт спортивного оборудования\r\n', '', 7, NULL, NULL),
(145, 'Спорт\r\n', '', 7, NULL, NULL),
(146, 'Спортивные секции\r\n', '', 7, NULL, NULL),
(147, 'Спортивный инвентарь\r\n', '', 7, NULL, NULL),
(148, 'Тренажерные залы\r\n', '', 7, NULL, NULL),
(149, 'Турагентства\r\n', '', 7, NULL, NULL),
(150, 'Туризм\r\n', '', 7, NULL, NULL),
(151, 'Фитнес-клубы\r\n', '', 7, NULL, NULL),
(152, 'Хостелы\r\n', '', 7, NULL, NULL),
(153, 'Автошколы\r\n', '', 8, NULL, NULL),
(154, 'Курсы иностранного языка\r\n', '', 8, NULL, NULL),
(155, 'Курсы творчества и рукоделия\r\n', '', 8, NULL, NULL),
(156, 'Музыкальные курсы\r\n', '', 8, NULL, NULL),
(157, 'Повышение квалификации\r\n', '', 8, NULL, NULL),
(158, 'Саморазвитие\r\n', '', 8, NULL, NULL),
(159, 'Танцевальные студии\r\n', '', 8, NULL, NULL),
(160, 'Творческие мастерские\r\n', '', 8, NULL, NULL),
(161, 'Тренинги,семинары и курсы\r\n', '', 8, NULL, NULL),
(162, 'Центры развития детей\r\n', '', 8, NULL, NULL),
(163, 'Аренда\r\n', '', 9, NULL, NULL),
(164, 'Благоустройство\r\n', '', 9, NULL, NULL),
(165, 'Дизайн интерьеров\r\n', '', 9, NULL, NULL),
(166, 'Ландшафтный дизайн\r\n', '', 9, NULL, NULL),
(167, 'Недвижимость\r\n', '', 9, NULL, NULL),
(168, 'Окна и двери\r\n', '', 9, NULL, NULL),
(169, 'Ремонт\r\n', '', 9, NULL, NULL),
(170, 'Спецтехника\r\n', '', 9, NULL, NULL),
(171, 'Строительные материалы\r\n', '', 9, NULL, NULL),
(172, 'Строительные работы\r\n', '', 9, NULL, NULL),
(173, 'Сырье\r\n', '', 9, NULL, NULL),
(174, 'Автоюристы\r\n', '', 10, NULL, NULL),
(175, 'Банки\r\n', '', 10, NULL, NULL),
(176, 'Безопасность\r\n', '', 10, NULL, NULL),
(177, 'Консалтинг\r\n', '', 10, NULL, NULL),
(178, 'Нотариальные конторы\r\n', '', 10, NULL, NULL),
(179, 'Страховые компании\r\n', '', 10, NULL, NULL),
(180, 'Юристы\r\n', '', 10, NULL, NULL),
(181, 'Аренда\r\n', '', 11, NULL, NULL),
(182, 'Безопасность\r\n', '', 11, NULL, NULL),
(183, 'Вендинг\r\n', '', 11, NULL, NULL),
(184, 'Дизайн и полиграфия\r\n', '', 11, NULL, NULL),
(185, 'Кейтеринг\r\n', '', 11, NULL, NULL),
(186, 'Клининг\r\n', '', 11, NULL, NULL),
(187, 'Консалтинг\r\n', '', 11, NULL, NULL),
(188, 'Логистика\r\n', '', 11, NULL, NULL),
(189, 'Медицинское оборудование\r\n', '', 11, NULL, NULL),
(190, 'Недвижимость\r\n', '', 11, NULL, NULL),
(191, 'Полиграфия и печать\r\n', '', 11, NULL, NULL),
(192, 'Реклама в СМИ\r\n', '', 11, NULL, NULL),
(193, 'Реклама в интернете/SEO\r\n', '', 11, NULL, NULL),
(194, 'Рекламные агентства\r\n', '', 11, NULL, NULL),
(195, 'Спецодежда\r\n', '', 11, NULL, NULL),
(196, 'Спецтехника\r\n', '', 11, NULL, NULL),
(197, 'Строительные работы\r\n', '', 11, NULL, NULL),
(198, 'Сырье\r\n', '', 11, NULL, NULL),
(199, 'Телекоммуникации\r\n', '', 11, NULL, NULL),
(200, 'Товары для бизнеса\r\n', '', 11, NULL, NULL),
(201, 'Торговое оборудование\r\n', '', 11, NULL, NULL),
(202, 'Франшизы\r\n', '', 11, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breaf_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `companies`
--

INSERT INTO `companies` (`id`, `title`, `category_id`, `logo`, `breaf_description`, `phone`, `address`, `city_id`, `created_at`, `updated_at`) VALUES
(1, 'first company', 2, 'img/src/...', 'dedscription dedscription dedscription dedscription dedscription dedscription', 7777777, 'Baker street 221b', 3, NULL, NULL),
(2, 'test', 3, 'http://otau/img/1.png', 'descrp test descrp test descrp test descrp test descrp test descrp test descrp test descrp test ', 77777, 'arinov ', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `user_name` varchar(512) NOT NULL,
  `user_email` varchar(512) NOT NULL,
  `user_phone` int(11) NOT NULL,
  `subject` varchar(512) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_09_18_052845_create_categories_table', 2),
(6, '2018_09_18_085624_create_companies_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
